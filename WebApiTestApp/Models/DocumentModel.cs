﻿using System;

namespace WebApiTestApp.Models
{
    public class DocumentModel
    {
        public string Name { get; set; }
        public Guid DocumentId { get; set; }
        public byte[] Stream { get; set; }
    }
}