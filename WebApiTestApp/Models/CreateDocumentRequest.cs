﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTestApp.Models
{
    public class CreateDocumentRequest
    {
        public Guid UserId { get; set; }
        public string DocumentName { get; set; }
        public byte[] DocumentStream { get; set; }
    }
}
