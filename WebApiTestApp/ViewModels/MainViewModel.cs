﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using WebApiTestApp.Models;
using WebApiTestApp.Views;

namespace WebApiTestApp.ViewModels
{
    public class MainViewModel:NotificationObject
    {
        private List<DocumentViewModel> _documents;
        private bool _showLoadingAnimation;

        public MainViewModel()
        {
            GetDocumentsCommand = new DelegateCommand(DoGetDocuments);
            ConvertFileCommand = new DelegateCommand(DoConvertFile);
            Refresh();
        }

        private void DoConvertFile()
        {
            var createDocumentDialog=new CreateDocumentView();
            var dataContext = new ConvertDocumentViewModel();
            createDocumentDialog.DataContext = dataContext;
            dataContext.Created += dataContext_Created;
            createDocumentDialog.ShowDialog();
        }

        void dataContext_Created(object sender, EventArgs e)
        {
            var convertDocumentViewModel = sender as ConvertDocumentViewModel;
            if (convertDocumentViewModel != null)
                convertDocumentViewModel.Created -= dataContext_Created;
            Refresh();
        }


        private async void DoGetDocuments()
        {
            ShowLoadingAnimation = true;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9019/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // New code:
                var getDocumentsResponse = await client.GetAsync("api/Documents");

                if (getDocumentsResponse.IsSuccessStatusCode)
                {
                    Documents = getDocumentsResponse.Content.ReadAsAsync<IEnumerable<DocumentModel>>().Result.Select(DocumentAdaptor.DocumentModel2ViewModel).ToList();
                    foreach (var document in Documents)
                    {
                        document.Deleted += document_Deleted;
                    }
                }
            }
            ShowLoadingAnimation = false;
        }

        void document_Deleted(object sender, EventArgs e)
        {
            Refresh();
            var documentViewModel = sender as DocumentViewModel;
            if (documentViewModel != null) documentViewModel.Deleted -= document_Deleted;
        }

        public List<DocumentViewModel> Documents
        {
            get { return _documents; }
            private set
            {
                if (Equals(value, _documents)) return;
                _documents = value;
                RaisePropertyChanged(() => Documents);
            }
        }

        public DelegateCommand GetDocumentsCommand { get; private set; }

        public DelegateCommand ConvertFileCommand { get; private set; }

        public void Refresh()
        {
            DoGetDocuments();
        }

        public bool ShowLoadingAnimation
        {
            get { return _showLoadingAnimation; }
            set
            {
                if (value.Equals(_showLoadingAnimation)) return;
                _showLoadingAnimation = value;
                RaisePropertyChanged(() => ShowLoadingAnimation);
            }
        }
    }
}
