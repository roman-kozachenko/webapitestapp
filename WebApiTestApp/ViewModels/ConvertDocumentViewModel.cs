﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using WebApiTestApp.Models;

namespace WebApiTestApp.ViewModels
{
    public class ConvertDocumentViewModel : NotificationObject
    {
        #region Private fields

        private string _fileName;
        private Stream _fileStream;
        private string _documentName;
        private bool _showLoadingAnimation;

        #endregion
        
        #region Constructor

        public ConvertDocumentViewModel()
        {

            InitializeCommands();
        }

        #endregion


        #region Initialization

        private void InitializeCommands()
        {
            OpenFileCommand = new DelegateCommand(DoOpenFile);
            ConvertFileCommand = new DelegateCommand(DoConvertFile, CanConvertFile);
        }

        #endregion


        #region Command handlers

        private bool CanConvertFile()
        {
            return DocumentName != null && FileStream != null;
        }

        private async void DoConvertFile()
        {
            ShowLoadingAnimation = true;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9019/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // New code:
                var request = new CreateDocumentRequest()
                {
                    DocumentName = DocumentName,
                    DocumentStream = ReadFully(FileStream),
                    //TODO: UserId = 
                };
                try
                {
                    var getDocumentsResponse = await client.PostAsJsonAsync("api/Documents/", request);
                    if(getDocumentsResponse.IsSuccessStatusCode)
                        OnCreated();
                }
                catch (Exception e)
                {
                    
                }
            }
            ShowLoadingAnimation = false;
        }
        private static byte[] ReadFully(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        private void DoOpenFile()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog {DefaultExt = ".docx"};

            // Show open file dialog box 
            var result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                FileName= dlg.FileName;
                FileStream = dlg.OpenFile();
            }
        }

        #endregion


        #region Commands

        public DelegateCommand OpenFileCommand { get; private set; }
        public DelegateCommand ConvertFileCommand { get; private set; }
        public DelegateCommand GetDocumentsCommand { get; private set; }
        #endregion


        #region Public properties

        public string FileName
        {
            get { return _fileName; }
            set
            {
                if (value == _fileName) return;
                _fileName = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        #endregion

        private Stream FileStream
        {
            get { return _fileStream; }
            set
            {
                _fileStream = value;
                ConvertFileCommand.RaiseCanExecuteChanged();
            }
        }

        public string DocumentName
        {
            get { return _documentName; }
            set
            {
                if (value == _documentName) return;
                _documentName = value;
                RaisePropertyChanged(() => DocumentName); 
                ConvertFileCommand.RaiseCanExecuteChanged();
            }
        }

        public event EventHandler Created;

        protected virtual void OnCreated()
        {
            EventHandler handler = Created;
            if (handler != null) handler(this, EventArgs.Empty);
        }


        public bool ShowLoadingAnimation
        {
            get { return _showLoadingAnimation; }
            set
            {
                if (value.Equals(_showLoadingAnimation)) return;
                _showLoadingAnimation = value;
                RaisePropertyChanged(() => ShowLoadingAnimation);
            }
        }
    }
}
