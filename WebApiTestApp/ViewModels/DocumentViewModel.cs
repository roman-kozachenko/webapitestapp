﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using WebApiTestApp.Models;

namespace WebApiTestApp.ViewModels
{
    public class DocumentViewModel:NotificationObject
    {
        private MemoryStream _stream;
        private Guid _id;
        private MainViewModel _parentViewModel;

        public DocumentViewModel(string name, MemoryStream stream, Guid id)
        {
            Name = name;
            _stream = stream;
            _id = id;
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            SaveDocumentCommand = new DelegateCommand(DoSaveDocument);
            DeleteDocumenCommand = new DelegateCommand(DoDeleteDocument);
        }

        private async void DoDeleteDocument()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9019/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // New code:
                
                var getDocumentsResponse = await client.DeleteAsync("api/Documents/" + this._id);

                if (getDocumentsResponse.IsSuccessStatusCode)
                {
                    OnDeleted();
                }
            }
        }

        private async void DoSaveDocument()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9019/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // New code:
                var getDocumentsResponse = await client.GetAsync("api/Documents/"+this._id);

                if (getDocumentsResponse.IsSuccessStatusCode)
                {
                    var document = getDocumentsResponse.Content.ReadAsAsync<DocumentModel>().Result;
                    SaveFile(document);
                }
            }
        }

        private void SaveFile(DocumentModel document)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = document.Name; // Default file name
            dlg.DefaultExt = ".pdf"; // Default file extension
            dlg.Filter = "PDF documents (.pdf)|*.pdf"; // Filter files by extension

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                using (var file = dlg.OpenFile())
                {
                    file.Write(document.Stream, 0, document.Stream.Length);
                    file.Flush();
                }
                Process.Start(dlg.FileName);
            }
        }
        public string Name { get; set; }

        public DelegateCommand SaveDocumentCommand { get; private set; }
        public DelegateCommand DeleteDocumenCommand { get; private set; }
        public event EventHandler Deleted;

        protected virtual void OnDeleted()
        {
            EventHandler handler = Deleted;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}
