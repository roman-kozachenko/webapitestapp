﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTestApp.Models;
using WebApiTestApp.ViewModels;

namespace WebApiTestApp
{
    public static  class DocumentAdaptor
    {
        public static DocumentViewModel DocumentModel2ViewModel(DocumentModel arg)
        {
            var stream = new MemoryStream();
            new StreamWriter(stream).Write(arg.Stream);
            var result = new DocumentViewModel(string.IsNullOrWhiteSpace(arg.Name) ? "unnamed" : arg.Name, stream,
                arg.DocumentId);
            return result;
        }
    }
}
